#!/usr/bin/env python
# Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd. 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

print(sys.argv)
if len(sys.argv) != 3:
    print("Error: add_kernels.py requires 2 inputs!")
    exit(1)
kernel_file_path = sys.argv[1]
kernel_content = sys.argv[2]

if not os.path.exists(kernel_file_path):
  t=open(kernel_file_path,mode='w',encoding='utf-8')
  t.close()  
f=open(kernel_file_path,mode='a',encoding='utf-8')
f.write(kernel_content)
f.write('\n')
f.close()

  

  

