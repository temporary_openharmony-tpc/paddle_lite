#!/bin/bash
# Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd. 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

# Check input
if [ $# -lt  3 ];
then
    echo "Input error"
    echo "Usage:"
    echo "sh benchmark.sh <models_path> <repeats><warmup>"
    exit
fi

# Set benchmark params
MODELS_DIR=$1
WARMUP=$3
REPEATS=$2
NUM_THREADS_LIST=(1 2 4)
MODELS_LIST=$(ls $MODELS_DIR)

# Run benchmark
for threads in ${NUM_THREADS_LIST[@]}; do
    echo Threads=$threads Warmup=$WARMUP Repeats=$REPEATS
    for model_name in ${MODELS_LIST[@]}; do
      echo "Model=$model_name Threads=$threads"
          ./benchmark_bin ${MODELS_DIR}/$model_name 1 3 224 224 $REPEATS $WARMUP $threads 0
    done
done
