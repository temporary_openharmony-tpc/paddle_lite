/*
# Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/

#ifndef LITE_API_TOOLS_BENCHMARK_LIGHT_H_
#define LITE_API_TOOLS_BENCHMARK_LIGHT_H_
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

#include "lite/api/paddle_api.h"
#include "lite/utils/model_util.h"

class PerfData {
 public:
  void init(const int repeats) { repeats_ = repeats; }
  const float init_time() const { return init_time_; }
  const float first_time() const { return run_time_.at(0); }
  const float avg_pre_process_time() const {
    return std::accumulate(pre_process_time_.end() - repeats_,
                           pre_process_time_.end(),
                           0.f) /
           repeats_;
  }
  const float min_pre_process_time() const {
    return *std::min_element(pre_process_time_.end() - repeats_,
                             pre_process_time_.end());
  }
  const float max_pre_process_time() const {
    return *std::max_element(pre_process_time_.end() - repeats_,
                             pre_process_time_.end());
  }
  const float avg_post_process_time() const {
    return std::accumulate(post_process_time_.end() - repeats_,
                           post_process_time_.end(),
                           0.f) /
           repeats_;
  }
  const float min_post_process_time() const {
    return *std::min_element(post_process_time_.end() - repeats_,
                             post_process_time_.end());
  }
  const float max_post_process_time() const {
    return *std::max_element(post_process_time_.end() - repeats_,
                             post_process_time_.end());
  }
  const float avg_run_time() const {
    return std::accumulate(run_time_.end() - repeats_, run_time_.end(), 0.f) /
           repeats_;
  }
  const float min_run_time() const {
    return *std::min_element(run_time_.end() - repeats_, run_time_.end());
  }
  const float max_run_time() const {
    return *std::max_element(run_time_.end() - repeats_, run_time_.end());
  }

  void set_init_time(const float ms) { init_time_ = ms; }
  void set_pre_process_time(const float ms) { pre_process_time_.push_back(ms); }
  void set_post_process_time(const float ms) {
    post_process_time_.push_back(ms);
  }
  void set_run_time(const float ms) { run_time_.push_back(ms); }

 private:
  int repeats_{0};
  float init_time_{0.f};
  std::vector<float> pre_process_time_;
  std::vector<float> post_process_time_;
  std::vector<float> run_time_;
};

void Run(const std::string& model_file,
         const paddle::lite_api::shape_t& input_shape,
         const int repeats,
         const int warmup,
         const int threads,
         const int power_mode);

int64_t ShapeProduction(const paddle::lite_api::shape_t& shape) {
  int64_t res = 1;
  for (auto i : shape) res *= i;
  return res;
}
#endif  // LITE_API_TOOLS_BENCHMARK_LIGHT_H_
